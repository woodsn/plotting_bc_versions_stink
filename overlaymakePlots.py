#!/usr/bin/python
import ROOT
#import matplotlib.pyplot as plt
from ROOT import gROOT
#from ROOT import SetOwnership
#SetOwnership( legend, 0 )
#from rootpy.plotting import histogram, Canvas
#gDirectory = gROOT.GetGlobal("gDirectory")
#####   RSG   #####
ROOT.gStyle.SetOptStat(0)
f = ROOT.TFile.Open("/export/share/gauss/woodsn/Ismet_CxAODReader/run/mc16_13TeV.302193.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_VcWZ_lvqq_m0700/data-MVATree/data-CxAOD.root")
t = f.Get("Nominal")

#####   tt    #######
g = ROOT.TFile.Open("/export/share/gauss/woodsn/Ismet_CxAODReader/run/ttbar_jan31/data-MVATree/data-CxAOD.root")
u = g.Get("Nominal")

c1 = ROOT.TCanvas("c1", "c1", 0, 0, 1000, 600)
variables = ['partonLabel', 'numTrkpt500', 'LeppT', 'MET', 'NPV', 'LepEta', 'WhadMass', 'WhadpT', 'WlepMass', 'WleppT', 'etaj', 'ptj', 'phij']
bins = [26, 25, 100, 100, 30, 100, 100, 80, 10, 80, 30, 50, 30]
mins = [-1, 0, 0, 0, 0, -3.2, 40E3, 0, 70E3, 100, -3, 0, -3]
maxs = [25, 25, 400E3, 600E3, 30, 3.2, 220E3, 800E3, 85E3, 400E3, 3, 700, 3]

for i in range(len(variables)):
	print "i is" + variables[i]
	title = variables[i]
	hist1 = ROOT.TH1F("hist1","", bins[i], mins[i], maxs[i])
	hist3 = ROOT.TH1F("hist3","", bins[i], mins[i], maxs[i])
	makehisto = variables[i] + ">>hist1"
	makehisto2 = variables[i] + ">>hist2"
	makehisto3 = variables[i] + ">>hist3"
	makehisto4 = variables[i] + ">>hist4"
#	makehisto = variables[i] + ">>hist1"
#	makehisto2 = variables[i] + ">>hist2"
#	makehisto3 = variables[i] + ">>hist3"
#	makehisto4 = variables[i] + ">>hist4"
	t.Draw(makehisto,"weightResolved*1","goff")
	t.Draw(makehisto3,"weightResolved*pass_ggFResolved_WZ_SR","goff")

	print 'signal preSelection Integral: ' + str(hist1.Integral())
	print 'signal full selection Integral: ' + str(hist3.Integral())
	hist1.Scale(1/hist1.GetEntries())
	hist3.Scale(1/hist3.GetEntries())
	hist2 = ROOT.TH1F("hist2","", bins[i], mins[i], maxs[i])
	hist4 = ROOT.TH1F("hist4","", bins[i], mins[i], maxs[i])
	u.Draw(makehisto2,"weightResolved*1","goff")
	u.Draw(makehisto4,"weightResolved*pass_ggFResolved_WZ_SR","goff")
	print 'background preselection Integral: ' + str(hist2.Integral())
	print 'background full selection Integral: ' + str(hist4.Integral())
	hist2.SetLineColor(2)
	hist3.SetLineStyle(7)
	hist4.SetLineStyle(7)
	hist4.SetLineColor(2)
	hist2.Scale(1/(hist2.Integral()))
	hist4.Scale(1/(hist4.Integral()))
	hist1.SetTitle(variables[i])
	hist2.SetTitle(variables[i])
	hist3.SetTitle(variables[i])
	hist4.SetTitle(variables[i])



	if hist1.GetMaximum() > hist2.GetMaximum() and hist1.GetMaximum() > hist3.GetMaximum() and hist1.GetMaximum() > hist4.GetMaximum():
		print 'hist1>hist2'
		hist1.Draw("HIST")
		hist2.Draw("HIST SAME")
		hist3.Draw("HIST SAME")
		hist4.Draw("HIST SAME")
	elif hist2.GetMaximum() > hist3.GetMaximum() and hist2.GetMaximum() > hist4.GetMaximum():
		hist2.Draw("HIST")
		hist1.Draw("HIST SAME")
		hist3.Draw("HIST SAME")
		hist4.Draw("HIST SAME")
	elif hist3.GetMaximum() > hist4.GetMaximum():
		hist3.Draw("HIST")
		hist1.Draw("HIST SAME")
		hist2.Draw("HIST SAME")
		hist4.Draw("HIST SAME")
	else:	
		hist4.Draw("HIST")
		hist1.Draw("HIST SAME")
		hist3.Draw("HIST SAME")
		hist2.Draw("HIST SAME")

	name = variables[i] + ".pdf"
	c1.Print(name)


hist_q = ROOT.TH2F("hist_q", "ttbar quark: eta_j vs phi_j", 32, -4, 4, 32, -4, -4)
#u.Project("hist_q", "etaj:phij")
u.Draw("phij:etaj>>hist_q", "weightResolved*(partonLabel > 0 && partonLabel < 7)")
hist_q.Draw("colz")
c1.Print('ttbar_quark_heatmap.pdf')


hist_g = ROOT.TH2F("hist_g", "ttbar gluon: eta_j vs phi_j", 32, -4, 4, 32, -4, -4)
#u.Project("hist_q", "etaj:phij")
u.Draw("phij:etaj>>hist_g", "weightResolved*(partonLabel ==21)")
hist_g.Draw("colz")
c1.Print('ttbar_gluon_heatmap.pdf')

hist_q_sig = ROOT.TH2F("hist_q_sig", "signal quark: eta_j vs phi_j", 32, -4, 4, 32, -4, -4)
#u.Project("hist_q", "etaj:phij")
u.Draw("phij:etaj>>hist_q_sig", "weightResolved*(partonLabel > 0 && partonLabel < 7)")
hist_q_sig.Draw("colz")
c1.Print('sig_quark_heatmap.pdf')


hist_g_sig = ROOT.TH2F("hist_g_sig", "sig gluon: eta_j vs phi_j", 32, -4, 4, 32, -4, -4)
#u.Project("hist_q", "etaj:phij")
u.Draw("phij:etaj>>hist_g_sig", "weightResolved*(partonLabel ==21)")
hist_g_sig.Draw("colz")
c1.Print('sig_gluon_heatmap.pdf')









variables2 = ['ptj', 'etaj', 'numTrkpt500']
bins = [50, 30, 40]
mins = [0, -3, 0]
maxs = [700, 3, 40]
ptbins = [50, 100, 150, 200, 250, 10000]
color = 1




for i in range(len(variables2)):
	print "i is " + variables2[i] + ". i actually is: " + str(i)

	hist50_q = ROOT.TH1F("hist50_q","", bins[i], mins[i], maxs[i])
	hist50_g = ROOT.TH1F("hist50_g","", bins[i], mins[i], maxs[i])
	hist100_q = ROOT.TH1F("hist100_q","", bins[i], mins[i], maxs[i])
	hist100_g = ROOT.TH1F("hist100_g","", bins[i], mins[i], maxs[i])
	hist150_q = ROOT.TH1F("hist150_q","", bins[i], mins[i], maxs[i])
	hist150_g = ROOT.TH1F("hist150_g","", bins[i], mins[i], maxs[i])
	hist200_q = ROOT.TH1F("hist200_q","", bins[i], mins[i], maxs[i])
	hist200_g = ROOT.TH1F("hist200_g","", bins[i], mins[i], maxs[i])
	hist250_q = ROOT.TH1F("hist250_q","", bins[i], mins[i], maxs[i])
	hist250_g = ROOT.TH1F("hist250_g","", bins[i], mins[i], maxs[i])
	
	histonames_q = ['hist50_q', 'hist100_q', 'hist150_q', 'hist200_q', 'hist250_q']
	histonames_g = ['hist50_g', 'hist100_g', 'hist150_g', 'hist200_g', 'hist250_g']
	
	for j in range(len(ptbins) - 1):
		print 'j is: ' + str(j)
		ptstring_quark = 'weightResolved*(ptj > ' + str(ptbins[j]) + ' && ptj < ' + str(ptbins[j+1]) + " && partonLabel > 0 && partonLabel < 7)"
		ptstring_gluon = 'weightResolved*(ptj > ' + str(ptbins[j]) + ' && ptj < ' + str(ptbins[j+1]) + " && partonLabel ==21)"
		makehisto2 = variables2[i] + '>>' + histonames_q[j]
		makehisto4 = variables2[i] + '>>' + histonames_g[j]
		u.Draw(makehisto2,ptstring_quark,"goff")
		u.Draw(makehisto4,ptstring_gluon,"goff")




	print "histonames_q_50 Integral: " + str(hist50_q.Integral())
	print "histonames_g_50 Integral: " + str(hist50_g.Integral())
	print "histonames_q_100 Integral: " + str(hist100_q.Integral())
	print "histonames_g_100 Integral: " + str(hist100_g.Integral())
	print "histonames_q_150 Integral: " + str(hist150_q.Integral())
	print "histonames_g_150 Integral: " + str(hist150_g.Integral())
	print "histonames_q_200 Integral: " + str(hist200_q.Integral())
	print "histonames_g_200 Integral: " + str(hist200_g.Integral())
	print "histonames_q_250 Integral: " + str(hist250_q.Integral())
	print "histonames_g_250 Integral: " + str(hist250_g.Integral())



	hist50_q.SetLineColor(1)
	hist50_q.SetLineWidth(2)
	hist50_g.SetLineWidth(2)
	hist50_g.SetLineStyle(7)
	hist50_g.SetLineColor(1)
	if hist50_q.GetEntries()!=0:
		hist50_q.Scale(1/hist50_q.Integral())
	if hist50_q.GetEntries()==0:
		hist50_q.Scale(0)
	if hist50_g.GetEntries()!=0:
		hist50_g.Scale(1/hist50_g.Integral())
	if hist50_g.GetEntries()==0:
		hist50_g.Scale(0)
	hist50_q.SetTitle(variables2[i])
	hist50_g.SetTitle(variables2[i])

	hist100_q.SetLineColor(2)
	hist100_q.SetLineWidth(2)
	hist100_g.SetLineWidth(2)
	hist100_g.SetLineStyle(7)
	hist100_g.SetLineColor(2)
	if hist100_q.GetEntries()!=0:
		hist100_q.Scale(1/hist100_q.Integral())
	if hist100_q.GetEntries()==0:
		hist100_q.Scale(0)
	if hist100_g.GetEntries()!=0:
		hist100_g.Scale(1/hist100_g.Integral())
	if hist100_g.GetEntries()==0:
		hist100_g.Scale(0)
	hist100_q.SetTitle(variables2[i])
	hist100_g.SetTitle(variables2[i])


	hist150_q.SetLineColor(3)
	hist150_q.SetLineWidth(2)
	hist150_g.SetLineWidth(2)
	hist150_g.SetLineStyle(7)
	hist150_g.SetLineColor(3)
	if hist150_q.GetEntries()!=0:
		hist150_q.Scale(1/hist150_q.Integral())
	if hist150_q.GetEntries()==0:
		hist150_q.Scale(0)
	if hist150_g.GetEntries()!=0:
		hist150_g.Scale(1/hist150_g.Integral())
	if hist150_g.GetEntries()==0:
		hist150_g.Scale(0)
	hist150_q.SetTitle(variables2[i])
	hist150_g.SetTitle(variables2[i])


	hist200_q.SetLineColor(4)
	hist200_q.SetLineWidth(2)
	hist200_g.SetLineWidth(2)
	hist200_g.SetLineStyle(7)
	hist200_g.SetLineColor(4)
	if hist200_q.GetEntries()!=0:
		hist200_q.Scale(1/hist200_q.Integral())
	if hist200_q.GetEntries()==0:
		hist200_q.Scale(0)
	if hist200_g.GetEntries()!=0:
		hist200_g.Scale(1/hist200_g.Integral())
	if hist200_g.GetEntries()==0:
		hist200_g.Scale(0)
	hist200_q.SetTitle(variables2[i])
	hist200_g.SetTitle(variables2[i])


	hist250_q.SetLineColor(6)
	hist250_q.SetLineWidth(2)
	hist250_g.SetLineWidth(2)
	hist250_g.SetLineStyle(7)
	hist250_g.SetLineColor(6)
	if hist250_q.GetEntries()!=0:
		hist250_q.Scale(1/hist250_q.Integral())
	if hist250_q.GetEntries()==0:
		hist250_q.Scale(0)
	if hist250_g.GetEntries()!=0:
		hist250_g.Scale(1/hist250_g.Integral())
	if hist250_g.GetEntries()==0:
		hist250_g.Scale(0)
	hist250_q.SetTitle(variables2[i])
	hist250_g.SetTitle(variables2[i])







#	print "histonames_q[j] Integral: " + str(hist50_q.Integral())
#	print "histonames_g[j] Integral: " + str(hist50_g.Integral())
#	print "histonames_q[j] Integral: " + str(hist100_q.Integral())
#	print "histonames_g[j] Integral: " + str(hist100_g.Integral())
#	print "histonames_q[j] Integral: " + str(hist150_q.Integral())
#	print "histonames_g[j] Integral: " + str(hist150_g.Integral())
	
	max1 = hist50_q.GetMaximum()
	max2 = hist50_g.GetMaximum()
	max3 = hist100_q.GetMaximum()
	max4 = hist100_g.GetMaximum()
	max5 = hist150_q.GetMaximum()
	max6 = hist150_g.GetMaximum()
	max7 = hist200_q.GetMaximum()
	max8 = hist200_g.GetMaximum()
	max9 = hist250_q.GetMaximum()
	max10 = hist250_g.GetMaximum()

	print "max1: " +str(max1)
	print "max2: " +str(max2)
	print "max3: " +str(max3)
	print "max4: " +str(max4)
	print "max5: " +str(max5)
	print "max6: " +str(max6)
	print "max7: " +str(max7)
	print "max8: " +str(max8)
	print "max9: " +str(max9)
	print "max10: " +str(max10)

	if max1>max2 and max1>max3 and max1>max4 and max1>max5 and max1>max6 and max1>max7 and max1>max8 and max1>max9 and max1>max10:
		hist50_q.Draw("HIST");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max2>max3 and max2>max4 and max2>max5 and max2>max6 and max2>max7 and max2>max8 and max2>max9 and max2>max10:
		hist50_g.Draw("HIST");
		hist50_q.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max3>max4 and max3>max5 and max3>max6 and max3>max7 and max3>max8 and max3>max9 and max3>max10:
		hist100_q.Draw("HIST");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max4>max5 and max4>max6 and max4>max7 and max4>max8 and max4>max9 and max4>max10:
		print 'HERE'
		hist100_g.Draw("HIST");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max5>max6 and max5>max7 and max5>max8 and max5>max9 and max5>max10:
		hist150_q.Draw("HIST");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max6>max7 and max6>max8 and max6>max9 and max6>max10:
		hist150_g.Draw("HIST");
		hist150_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");


	elif max7>max8 and max7>max9 and max7>max10:
		hist200_q.Draw("HIST");
		hist150_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");

	elif max8>max9 and max8>max10:
		hist200_g.Draw("HIST");
		hist200_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist250_q.Draw("HIST same");
		hist250_g.Draw("HIST same");


	elif max9>max10:
		hist250_q.Draw("HIST");
		hist200_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");
		hist250_g.Draw("HIST same");


	else:
		hist250_g.Draw("HIST");
		hist250_q.Draw("HIST same");
		hist200_g.Draw("HIST same");
		hist200_q.Draw("HIST same");
		hist150_g.Draw("HIST same");
		hist150_q.Draw("HIST same");
		hist100_g.Draw("HIST same");
		hist50_q.Draw("HIST same");
		hist50_g.Draw("HIST same");
		hist100_q.Draw("HIST same");


	name = variables2[i] + "_ptslice.pdf"
	c1.SaveAs(name)
	color+=1

