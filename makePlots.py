#!/usr/bin/python
import ROOT
#import matplotlib.pyplot as plt
from ROOT import gROOT
#from ROOT import SetOwnership
#SetOwnership( legend, 0 )
#from rootpy.plotting import histogram, Canvas
#gDirectory = gROOT.GetGlobal("gDirectory")
#####   RSG   #####
f = ROOT.TFile.Open("/export/share/gauss/woodsn/Ismet_CxAODReader/RSGWW_700/data-MVATree/data-CxAOD.root")
t = f.Get("Nominal")

#####   tt    #######
g = ROOT.TFile.Open("/export/share/gauss/woodsn/Ismet_CxAODReader/ttbarnew/data-MVATree/data-CxAOD.root")
u = g.Get("Nominal")

c1 = ROOT.TCanvas("c1", "c1", 0, 0, 1000, 600)
variables = ['partonLabel', 'numTrkpt500', 'LeppT', 'MET', 'NPV', 'LepEta', 'WhadMass', 'WhadpT', 'WlepMass', 'WleppT']
bins = [26, 35, 100, 100, 30, 100, 100, 80, 100, 80]
mins = [-1, 0, 0, 0, 0, -3.2, 40E3, 0, 80E3, 100]
maxs = [25, 40, 400E3, 600E3, 30, 3.2, 220E3, 800E3, 100E3, 400E3]

for i in range(len(variables)):
	print "i is" + variables[i]
	title = variables[i]
	hist1 = ROOT.TH1F("hist1","", bins[i], mins[i], maxs[i])
	makehisto = variables[i] + ">>hist1"
	makehisto2 = variables[i] + ">>hist2"
	t.Draw(makehisto,"pass_ggFResolved_WW_SR","goff")
	hist1.Scale(1/hist1.GetEntries())
	hist2 = ROOT.TH1F("hist2","", bins[i], mins[i], maxs[i])
	u.Draw(makehisto2,"pass_ggFResolved_WW_SR","goff")
	hist2.SetLineColor(2)
	hist2.Scale(1/(hist2.GetEntries()))
	hist2.SetTitle(variables[i])
	hist1.SetTitle(variables[i])


#	legend=ROOT.TLegend(0.6,0.6,0.8,0.8)
#	legend.AddEntry(hist1, hist1.GetName(), "l")
#	legend.AddEntry(hist2, hist2.GetName(), "l")
#	legend.Draw()
	if hist1.GetMaximum() > hist2.GetMaximum():
		print 'hist1>hist2'
		hist1.Draw()
		hist2.Draw("SAME")
	else:	
		print 'hist2>hist1'
		hist2.Draw()
		hist1.Draw("SAME")

	name = variables[i] + ".pdf"
	c1.Print(name)



#hist1 = ROOT.TH1F("hist1","", 30,-5, 25)
#t.Draw("partonLabel>>hist1","","goff")
#hist1.Scale(1/hist1.GetEntries())
#hist2 = ROOT.TH1F("hist2","", 30,-5, 25)
#u.Draw("partonLabel>>hist2","","goff")
#hist2.SetLineColor(2)
#hist2.Scale(1/(hist2.GetEntries()))
#hist2.Draw()
#hist1.Draw("SAME")
#c1.Print("partonLabel.pdf")























#partontruth=[]

#for event in t :
#	partontruth.append(event.partonLabel[0])
#	partontruth.append(event.partonLabel[1])
#	print event.partonLabel[0]
#	print event.partonLabel[1]



#t.Draw("leptonPt>>hist")
#u.Draw("leptonPt")
#leptonPt_sig = gDirectory.Get("hist")
#leptonPt_sig.Draw()
#hist.Draw()
#c1.Print("leptonPt.pdf")

#t.Draw("NPV")
#c1.Print("NPV.pdf")

#t.Draw("numTrkpt500")
#c1.Print("numTrkpt500.pdf")

#t.Draw("partonLabel")
#c1.Print("partonLabel.pdf")









