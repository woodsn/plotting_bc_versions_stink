#
# file: setup.sh
# author: Ryan Reece <ryan.reece@cern.ch>
# created: August 17, 2012
#
# Setup script for a release of MyAtlasAnalysisProject
#
###############################################################################


##-----------------------------------------------------------------------------
## pre-setup helpers, don't touch
##-----------------------------------------------------------------------------

SVN_USER=${SVN_USER:-$USER} # set SVN_USER to USER if not set

path_of_this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
path_above_this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
export ANALYSIS_PROJECT=${path_of_this_dir}

add_to_python_path()
{
    export PYTHONPATH=$1:$PYTHONPATH
    echo "  Added $1 to your PYTHONPATH."
}

add_to_path()
{
    export PATH=$1:$PATH
    echo "  Added $1 to your PATH."
}


##-----------------------------------------------------------------------------
## setupATLAS
##-----------------------------------------------------------------------------

echo "  Running setupATLAS."
setupATLAS

#localSetupGcc gcc484_x86_64_slc6
#localSetupPython 2.7.4-x86_64-slc6-gcc48
#localSetupROOT 6.02.12-x86_64-slc6-gcc48-opt
source  /etc/setup-scripts/setup-cvmfs-root6.sh

if [ -f rcSetup.sh ]; then
    source rcSetup.sh
else
#    rcSetup Base,2.3.34
#    rcSetup SUSY,2.3.39
#    rcSetup Base,2.4.12
#    rcSetup Base,2.4.21
#    rcSetup Base,2.4.22
#    rcSetup Base,2.4.23
#    rcSetup Base,2.4.28
    rcSetup Base,2.4.29
fi 

echo "  done."


##-----------------------------------------------------------------------------
## install SUSYTools
##-----------------------------------------------------------------------------

if [ ! -d SUSYTools ]; then
    echo "  Checking-out SUSYTools"
#    rc checkout_pkg SUSYTools
#    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-18
#    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-19
#    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-24
#    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-33
#    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-54
    rc checkout_pkg svn+ssh://${SVN_USER}@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/tags/SUSYTools-00-08-58
    echo "  done."
    echo ""
    echo "  Checking-out extra SUSYTools packages:"
    echo "rc checkout SUSYTools/doc/packages.txt"
    rc checkout SUSYTools/doc/packages.txt
    echo "  done."
    echo ""
    echo "  Checking-out my extra packages:"
    echo "rc checkout NPPAnalysis/data/packages.txt"
    rc checkout NPPAnalysis/data/packages.txt
    echo "  done."
    echo ""
    echo "  Applying hacks to SUSYTools"
    echo "  cp -f NPPAnalysis/hacks/SUSYToolsInit.cxx SUSYTools/Root/SUSYToolsInit.cxx"
    cp -f NPPAnalysis/hacks/SUSYToolsInit.cxx SUSYTools/Root/SUSYToolsInit.cxx
    echo "  cp -f NPPAnalysis/hacks/Muons.cxx SUSYTools/Root/Muons.cxx"
    cp -f NPPAnalysis/hacks/Muons.cxx SUSYTools/Root/Muons.cxx
    echo "  done."
    echo ""
    echo "  As this is your first time to setup the analysis code,"
    echo "  you need to build it by:"
    echo ""
    echo "  rc find_packages"
    echo "  rc clean"
    echo "  rc compile"
    echo ""
fi


##-----------------------------------------------------------------------------
## install other packaages
##-----------------------------------------------------------------------------

## root2html
#if [ ! -d root2html ]; then
#    echo "  Checking-out root2html"
#    git clone https://github.com/rreece/root2html.git
#fi


##-----------------------------------------------------------------------------
## setup PYTHONPATH
##-----------------------------------------------------------------------------

echo "  Setting up your PYTHONPATH."
add_to_python_path ${ANALYSIS_PROJECT}
add_to_python_path ${ANALYSIS_PROJECT}/NPPAnalysis/python
echo "  done."


##-----------------------------------------------------------------------------
## setup PATH

echo "  Setting up your PATH."
add_to_path ${ANALYSIS_PROJECT}/NPPAnalysis/scripts
add_to_path ${ANALYSIS_PROJECT}/root2html
#add_to_path ${ANALYSIS_PROJECT}/samples/scripts
echo "  done."


